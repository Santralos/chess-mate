import asyncio
import websockets


class ChessSocket:
    callback: callable

    def __init__(self, callback):
        self.callback = callback

    def start_socket(self):
        start_server = websockets.serve(self.server, 'localhost', 5000)

        asyncio.get_event_loop().run_until_complete(start_server)
        print('Websocket started on localhost:5000')

        asyncio.get_event_loop().run_forever()

    async def server(self, websocket, path):
        async for message in websocket:
            self.callback(message)


if __name__ == '__main__':

    socket = ChessSocket(print)
    socket.start_socket()
