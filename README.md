# ChessMate
ChessMate is a tool to calculate the best next chess move for popular chess websites such as chess24.com and chess.com
It consists of a chrome extension, which scrapes the game info from the webpage, and a python program running a chess-engine (currently only stockfish and amyan) that calculates the next move. Communication between these two components is through a WebSocket.

## Requirements
*  Python 3.xx

## Installation
Download the repository and install the python part by running 'install.bat' or by manually installing the requirements.txt
```bash
pip install -r requirements.txt
```
For the chrome extension part:
1. Open up chrome://extensions/
1. Ensure that the "Developer mode" checkbox in the top right-hand corner is checked.
1. Click on Load unpacked and select the extension folder from the repository.

## Usage
Start the chess tool by running 'run_stockfish.bat' or 'run_amyan.bat' or through the commandline.
```bash
py main.py stockfish
```
Start your game, and click on the ChessMate icon in the toolbar (make sure you pin the chrome extension in your toolbar). 
Click on 'Open Board' and a window will open with the chessboard and the next move.

Have fun ;)
