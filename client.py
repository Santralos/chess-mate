from typing import Optional
import chess
import chess.svg
import chess.engine

from websocket import ChessSocket

BOARD_SVG = 'board.svg'
ENGINE_CONFIGS = {
    "stockfish": {
        "Contempt": 0,
        "Threads": 7,
        "Hash": 16,
        "Skill Level": 20,
        "Move Overhead": 30,
        "Slow Mover": 100
    },
    "amyan": None
}


class ChessClient:
    def __init__(self, engine_name):
        """
        Starts the engine and websocket
        """
        self.engine = chess.engine.SimpleEngine.popen_uci("engines/" + engine_name + ".exe")
        if engine_name in ENGINE_CONFIGS and ENGINE_CONFIGS[engine_name] is not None:
            self.engine.configure(ENGINE_CONFIGS[engine_name])

        print("Engine loaded:", engine_name)
        self.board = chess.Board()

        # Create & start socket
        self.socket = ChessSocket(self.parse_input)
        self.socket.start_socket()

    def perform_move(self, move) -> None:
        """
        Performs move on current board
        """
        self.board.push_san(move)

    def parse_input(self, input_moves) -> None:
        """
        Receives an input string containing all the moves.
        Splits the string, parses the moves and calculates the best next move.
        Then, the best move (arrow) is printed on the board and a svg file of the board is generated.
        """
        side = 0
        moves = input_moves.split("_")

        # Perform all previous moves to recreate board from moves
        for x in moves:
            if x == "clear":
                self.board.reset()
            elif x == "w":
                side = 0
            elif x == "b":
                side = 1
            else:
                self.perform_move(x)

        # Calculate best move and get square id's
        best_move = self.get_best_move()
        if best_move is None:
            print('No next best move, game over')
            return

        first_square = chess.SQUARE_NAMES.index(best_move[0:2])
        second_square = chess.SQUARE_NAMES.index(best_move[2:4])

        self.perform_move(best_move)
        print(best_move[0:2], best_move[2:4])

        # Writing board to svg
        text_file = open(f"extension/{BOARD_SVG}", "w")
        text_file.write(chess.svg.board(self.board, orientation=1 - side, arrows=[(first_square, second_square)]))
        text_file.close()

    def get_best_move(self) -> str:
        """
        Calculates and returns the best next move on the current board (uci format)
        """
        result = self.engine.play(self.board, chess.engine.Limit(time=0.1))
        if result.move is None:
            return None

        return result.move.uci()
