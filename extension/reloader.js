const interval = setInterval(refreshImage, 50);

function refreshImage(){
  const timestamp = new Date().getTime();
  const el = document.getElementById("board");
  const queryString = "?t=" + timestamp;
  el.src = "board.svg" + queryString;
}
