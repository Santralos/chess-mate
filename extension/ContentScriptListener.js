// Communicates with popup to receive sync signal
chrome.extension.onRequest.addListener(function(request, sender, sendResponse){
  if(request.sync){
    syncBoardMoves(true)
  }
});
