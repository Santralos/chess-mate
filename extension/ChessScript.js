/*
ChessMate Content Script for https://chess.com/
 */

let socket = new WebSocket('ws://localhost:5000')
let total_moves = -1;

// Start syncing when the page is loaded.
window.onload = (event) => {
  syncBoardMoves()
};

// Gets all of the moves from the game and sends them via the websocket.
function syncBoardMoves(){
  // Repeat this function every 100ms
  setTimeout(syncBoardMoves,100);

  // Only sync when there is a new move
  let moves = getMoves();
  if (total_moves === moves.length){
    return;
  }
  total_moves = moves.length;

  // Build a string from all of the moves and sending it over the websocket.
  let side = document.querySelector('.clock-black.clock-bottom') == null ? "w" : "b"
  let st_message =  "clear_" + side;
  for (move of moves){
    st_message += "_" + move.innerText
  }
  socket.send(st_message)
}

// Scrapes all of the moves from the games webpage.
function getMoves(){
  let ret = [];
  for (move of document.querySelectorAll('.move')){
    for (node of move.querySelectorAll('.node')){
      ret.push(node);
    }
  }
  return ret;
}