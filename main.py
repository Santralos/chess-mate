import sys
from client import ChessClient

if __name__ == "__main__":
    engine_name = sys.argv[1]
    client = ChessClient(engine_name)
